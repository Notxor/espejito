local modname = core.get_current_modname()
local path = core.get_modpath(modname) .. "/"

dofile(path .. "espejito.lua")
dofile(path .. "conversacion.lua")
dofile(path .. "teletransporte.lua")

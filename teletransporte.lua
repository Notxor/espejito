core.register_node("espejito:tp", {
      description = "Destino de teletransporte",
      is_ground_content = false,
      tiles = {"default_steel_block.png^default_glass.png"},
      stack_max = 64,
      light_source = 2,
      groups = {dig_immediate = 2},
      after_place_node = function(pos, placer, itemstack, pointed_thing)
	 local pname = placer:get_player_name()
	 local meta = placer:get_meta()
	 meta:set_string("espejito:tp_pos", core.pos_to_string(pos))
      end,
})

core.register_node("espejito:tp", {
    description = "Destino de teletransporte",
    is_ground_content = false,
    tiles = {"default_steel_block.png^default_glass.png"},
    stack_max = 64,
    light_source = 2,
    groups = {dig_immediate = 2},
    after_place_node = function(pos, placer, itemstack, pointed_thing)
       local pname = placer:get_player_name()
       local meta = placer:get_meta()
       meta:set_string("espejito:tp_pos", core.pos_to_string(pos))
    end,
})

dofile(core.get_modpath("espejito") .. "/conversacion.lua")
local C = conversacion

local pregunta_formspec = "size[5.75,3.5]"..
   "label[1,0.25;¿Qué desea vuestra excelencia?]"..
   "field[0.5,1.5;5.25,1;pregunta;Pregunta: ;]"..
   "button_exit[1.5,2.3;3,0.8;preguntar;Preguntar]"

core.register_node("espejito:node", {
      description = "Espejito mágico",
      drawtype = "signlike",
      tiles = {"espejito_textura.png"},
      paramtype2 = "wallmounted",
      selection_box = {
	 type = "wallmounted",
      },
      walkable = false,
      light_source = 4,
      sounds = {
	 dug = {name = "espejito_roto", gain = 1.0},
      },
      groups = {dig_immediate = 2},
      on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
	 -- Click derecho sobre el espejo para mostrar el formulario de pregunta
	 local pname = clicker:get_player_name()
	 core.show_formspec(pname, "pregunta_espejo", pregunta_formspec )
	 core.sound_play("espejito_uso", {gain = 1.0, pos = pos})
      end,
})

core.register_on_player_receive_fields(function(player, formname, fields)
      local nombre_jugador = player:get_player_name()
      local pregunta = fields.pregunta
      if pregunta ~= nil then
         -- información de depuración a la consola
         core.log("action", nombre_jugador .. " pregunta al espejito: " .. pregunta)
         if (formname == "pregunta_espejo" and pregunta ~= "") then
            core.chat_send_player(nombre_jugador, C(nombre_jugador, pregunta))
         end
      end
end)

local diamond
if core.get_modpath("mcl_core") then
   diamond = "mcl_core:diamond"
elseif core.get_modpath("default") then
   diamond = "default:diamond"
else
   diamond = "group:diamond"
end

core.register_craft({
      type = "shaped",
      output = "espejito:node",
      recipe = {
	 {diamond, diamond, diamond},
	 {"group:glass", "group:glass", "group:glass"}
      },
})

local frases = {
   ["meteo"] = {
      "No se por que me dices «{{pregunta}}»... si aquí siempre hace bueno.",
      "Pues ha quedado buena tarde...",
   },
   ["hora"] = {
      "Son las {{hora}}",
      "¡Uy, que tarde! Si ya son las {{hora}}",
      "Las {{hora}}... ¡No son horas de estar hablando conmigo!",
   },
   ["saludo"] = {
      "«{{pregunta}}» para ti tambien.",
      "Hola, ¿que tal, {{jugador}}?",
      "Que pasa, {{jugador}}?",
   },
   ["tonto"] = {
      "«{{espejo}}»... ¿que esperabas?",
      "Soy una «estupidez artificial», no necesitas recordarme que {{espejo}}"
   },
   ["amistad"] = {
      "La amistad es muy importante",
      "Los amigos son los hermanos que elegimos, pero tienes que tener cuidado con quién te hermanas",
      "Quien tiene un amigo tiene un tesoro",
   },
   ["futuro"] = {
      "Nadie sabe lo que será, el futuro es incierto y el pasado se fue. Sólo te queda hacer lo que puedas para convertirte en lo que quieres",
      "El futuro es una ilusión, pero no se hace solo",
      "Nadie puede vivir del futuro",
   },
   ["sin_tema"] =  {
      "Cuñaaaaao...",
      "No puedorrr, no puedorrr...",
      "¡¿Que si quiere bolsa?!",
      "¿Puedes explicarlo un poco mejor?",
   },
}

local espejos = {
   ["yo"] = "tú",
   ["tú"] = "yo",
   ["mi"] = "tu",
   ["tu"] = "mi",
   ["me"] = "te",
   ["te"] = "me",
   ["conmigo"] = "contigo",
   ["contigo"] = "conmigo",
   ["dices"] = "digo",
   ["digo"] = "dices",
   ["soy"] = "eres",
   ["eres"] = "soy",
   ["pareces"] = "parezco",
   ["parezco"] = "pareces",
}

local function calcular_hora()
   local tiempo = core.get_timeofday() * 24
   local hora = math.floor(tiempo)
   local minutos = math.floor((tiempo - hora) * 60)
   return string.format("%d:%02d", hora, minutos)
end

local function determinar_tema(pregunta)
   -- Busca palabras clave en la frase para determinar el tema
   if string.find(pregunta, "nublado") or
      string.find(pregunta, "lluev") or
      string.find(pregunta, "llov") then
      return "meteo"
   elseif string.find(pregunta, "hora") then
      return "hora"
   elseif string.find(pregunta, "hola") or
      string.find(pregunta, "saludo") then
      return "saludo"
   elseif string.find(pregunta, "tont") then
      return "tonto"
   elseif string.find(pregunta, "amigo") or
      string.find(pregunta, "amiga") or
      string.find(pregunta, "amist") then
      return "amistad"
   elseif string.find(pregunta, "futuro") then
      return "futuro"
   end
   return "sin_tema"
end

local function tokenize(cadena, sep)
   local sep, palabras = sep or ":", {}
   local pattern = string.format("([^%s]+)", sep)
   cadena:gsub(pattern, function(c) palabras[#palabras+1] = c end)
   return palabras
 end

local function join(tabla, sep)
   local cadena = tabla[1]
   local len = #tabla
   if len == 0 then
      return ""
   end
   for i = 2, len do
      if tabla[i] ~= "" and tabla[i] ~= nil then
         cadena = cadena .. sep .. tabla[i]
      end
   end
   return cadena
end

local function espejar(cadena)
   -- Busca y sustituye en la `cadena` las cadenas de `espejo`
   local tabla = tokenize(cadena, " ")  -- convertimos la cadena en una lista de palabras
   for i, v in ipairs(tabla) do      -- sustituimos la palabra por su espejo, si existe
      local palabra = espejos[v]
      if palabra == nil then
         tabla[i] = v
      else
         tabla[i] = palabra
      end
   end
   return join(tabla, " ")   -- juntamos las palabras en una cadena de nuevo
end

local function procesar_respuesta(opciones, nombre_player, pregunta)
   -- sustituye algunas palabras en la frase para generar la respuesta
   local opcion = opciones[math.random(#opciones)]
   opcion = string.gsub(opcion, "{{jugador}}", nombre_player)
   opcion = string.gsub(opcion, "{{espejo}}", espejar(pregunta))
   return string.gsub(opcion, "{{pregunta}}", pregunta)
end

function conversacion(nombre_player, pregunta)
   -- Genera la conversación, primero necesitamos las frases
   -- Proceso de la respuesta por pasos:
   local tema = determinar_tema(pregunta)  -- Determinar el tema del que se habla
   local posibles = frases[tema]           -- Obtener las posibles respuestas según tema
   local respuesta = procesar_respuesta(posibles, nombre_player, pregunta)
   if tema == "sin_tema" then
      return "No tengo ni idea. " .. respuesta
   elseif tema == "hora" then
      return string.gsub(respuesta, "{{hora}}", calcular_hora())
   else
      core.log("action", "El espejito responde: ".. respuesta)
      return respuesta
   end
end
